<?php

use Illuminate\Database\Seeder;

class users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { 
        DB::table('users')->insert([
            'name' => 'Rafi',
            'email' => 'rafinyadi@gmail.com',
            'password' => bcrypt('123456')
            'remember_token' => NULL,
            'created_at' => 2017-08-29 18:44:33,
            'updated_at' => 2017-08-29 18:44:33,
            'deleted_at' => NULL
        ]);
    }
}
