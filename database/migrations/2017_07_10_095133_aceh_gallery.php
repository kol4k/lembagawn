<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AcehGallery extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aceh_gallery', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_gallery',11);
            $table->integer('id_album')->unsigned();
            $table->foreign('id_album')->references('id_album')->on('aceh_album');
            $table->text('gallery_keterangan');
            $table->string('gallery_gambar',191);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aceh_gallery');
    }
}
