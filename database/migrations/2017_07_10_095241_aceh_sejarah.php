<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AcehSejarah extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aceh_sejarah', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_sejarah',11);
            $table->string('sejarah_judul',191);
            $table->integer('id_kategori')->unsigned();
            $table->foreign('id_kategori')->references('id_kategori')->on('aceh_kategori')->onDelete('cascade');
            $table->text('sejarah_isi');
            $table->string('sejarah_gambar',191);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aceh_sejarah');
    }
}
