<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AcehArtikel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aceh_artikel', function (Blueprint $table) {
            $table->increments('id_artikel',11);
            $table->string('artikel_judul',191);
            $table->text('artikel_isi');
            $table->string('artikel_gambar',191);
            $table->string('password',191);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aceh_artikel');
    }
}
