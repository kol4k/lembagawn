<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AcehPustaka extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aceh_pustaka', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_pustaka',11);
            $table->integer('id_pkategori')->unsigned();
            $table->foreign('id_pkategori')->references('id_pkategori')->on('aceh_pkategori');
            $table->integer('id_penulis')->unsigned();
            $table->foreign('id_penulis')->references('id_penulis')->on('aceh_penulis');
            $table->string('pustaka_edisi',191);
            $table->date('pustaka_tanggal');
            $table->text('pustaka_deskripsi');
            $table->string('pustaka_gambar',191);
            $table->string('pustaka_file',191);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aceh_pustaka');
    }
}