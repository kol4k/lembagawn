<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AcehBudaya extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aceh_budaya', function (Blueprint $table) {
            $table->increments('id_budaya',11);
            $table->string('budaya_judul',191);
            $table->text('budaya_isi');
            $table->string('budaya_gambar',191);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aceh_budaya');
    }
}
