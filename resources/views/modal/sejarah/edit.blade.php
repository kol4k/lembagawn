<div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit Sejarah</h4>
                            </div>
                            <div class="modal-body">
                            <form class="form-horizontal" id="sejarah" method="POST" action="{{ route('sejarah.edit', $data->id_sejarah) }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="email" class="col-md-3 control-label">Judul</label>

                                <div class="col-md-7">
                                    <input id="nama" type="text" class="form-control" name="nama" value="{{ $data->sejarah_judul }}" required autofocus>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="email" class="col-md-3 control-label">Sejarah</label>

                                <div class="col-md-7">
                                    <textarea id="editor2" name="isi">{{ $data->sejarah_isi }}</textarea>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="kategori" class="col-md-3 control-label">Kategori</label>

                                <div class="col-md-4">
                                <select class="form-control" id="kategori" name="kategori">
                                @foreach($kategori as $kat)
                                    <option value="{{ $kat->id_kategori }}">{{ $kat->kategori_nama }}</option>
                                @endforeach
                                </select>
                            </div> 

                            <div class="form-group">
                                <label for="gambar" class="col-md-3 control-label">Upload Gambar</label>
                                
                                <div class="col-md-7">
                                    <input id="gambar" name="gambar" type="file">
                                </div>
                            </div>
                            
                            </div>
                            <div class="modal-footer">
                            <button type="submit" class="btn btn-default" form="sejarah">Submit</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                            </form>
                        </div>
                        </div>