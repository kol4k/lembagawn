<div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit Album</h4>
                            </div>
                            <div class="modal-body">
                            <form class="form-horizontal" id="album" method="POST" action="{{ route('album.edit', $data->id_album) }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="email" class="col-md-3 control-label">Nama</label>

                                <div class="col-md-7">
                                    <input id="judul" type="text" class="form-control" name="judul" value="{{ $data->album_judul }}" required autofocus>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="email" class="col-md-3 control-label">Keterangan</label>

                                <div class="col-md-7">
                                    <textarea class="form-control" name="keterangan" rows="3">{{ $data->album_keterangan }}</textarea>
                                </div>
                            </div>

                            </div>
                            <div class="modal-footer">
                            <button type="submit" class="btn btn-default" form="album">Submit</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                            </form>
                        </div>
                        </div>