<div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Profil # {{ $data->id_profil }}</h4>
                            </div>
                            <div class="modal-body">
                                <h4><b>{{ $data->profil_judul }}</b></h4>
                                <div class="span 4">
                                <img src="../shared/imgs/{{ $data->profil_gambar }}" class="rounded mx-auto d-block">
                                <p>{{ $data->profil_isi }}</p>
                                </div>
                            </div>
                            <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                        </div>