<div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">album # {{ $data->id_album }}</h4>
                            </div>
                            <div class="modal-body">
                                <h4>Album <b><i>{{ $data->Album->album_judul }}</i></b></h4>
                                <div class="span 4">
                                <p>{!! $data->gallery_keterangan !!}<br><br>
                                Waktu dibuat: {!! $data->created_at !!}
                                <img src="/shared/docs/{{ $data->gallery_files }}" class="img-responsive" alt="{{ $data->gallery_keterangan }}">
                                </p>
                                </div>
                            </div>
                            <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                        </div>