<div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit Kategori</h4>
                            </div>
                            <div class="modal-body">
                            <form class="form-horizontal" id="kategorisejarah" method="POST" action="{{ route('kategorisejarah.edit', $data->id_kategori) }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="email" class="col-md-3 control-label">Judul</label>

                                <div class="col-md-7">
                                    <input id="nama" type="text" class="form-control" name="nama" value="{{ $data->kategori_nama }}" required autofocus>
                                </div>
                            </div>
                            <div class="modal-footer">
                            <button type="submit" class="btn btn-default" form="kategorisejarah">Submit</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                            </form>
                        </div>
                        </div>