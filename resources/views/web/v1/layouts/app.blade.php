<html>
<head>
	<title>Lembaga Wali Naggroe Aceh</title>
    
    <!-- CSS -->
    <link href="/css/owl.css" rel="stylesheet">
    <link href="/css/nav.css" rel="stylesheet">
    <link href="/css/woo.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/bootstrap.css" rel="stylesheet" type="text/css">
    <!--<link href="/css/bootstrap-grid.css" rel="stylesheet" type="text/css">
    <link href="/css/bootstrap-reboot.css" rel="stylesheet" type="text/css">-->
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link href="/css/responsive.css" rel="stylesheet">
	<link href="./assets/css/jquery.jscrollpane.css" rel="stylesheet" >

    <script type="text/javascript" src="./assets/js/jquery.mousewheel.js"></script>
    <script type="text/javascript" src="./assets/js/jquery.jscrollpane.min.js"></script>
    <script src="/js/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <script src="/js/bootstrap.js"></script>
</head>
<body>
    <!-- Banner -->
        <nav class="navbar navbar-expand-lg ">
            <a class="navbar-brand" href="#">
                <img src="http://lembagawalinanggroe.id/assets/img/logo-header.png" width="150" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars" style="color: #848791;"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                <!--<ul class="navbar-nav">
                    
                </ul>-->
                <ul class="navbar-nav mx-auto">
                </ul>
                <ul class="navbar-nav menux">
                    <li class="nav-item user-account" id="nav-auth">
                        <form class="form-inline">
                            <button type="button" class="button btn_deal mr-sm-2">Profil</button>
                            <button type="button" class="button btn_deal mr-sm-2">Sejarah</button>
                            <button type="button" class="button btn_deal mr-sm-2">Kebudayaan</button>
                            <button type="button" class="button btn_deal mr-sm-2">Galeri</button>
                            <button type="button" class="button btn_deal mr-sm-2">Pustaka</button>
                        </form>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
<!--@yield('contentselector')-->
    @yield('content')
<div class="clear"></div>

	<footer id="colophon" class="site-footer" role="contentinfo">

				<div class="footer-newsletter support-tengah">
			<div class="container">
				<div class="row">
					<div class="col-sm-2"></div>
					<div class="col-sm-8">
						<h3 class="widget-title">Get Updates</h3>
						<p class="wow fadeIn animated" data-wow-delay=".25s">Get notified when we add new deals and set of freebies!</p>
						
                        <!-- Begin MailChimp Signup Form -->
                        <div id="mc_embed_signup">
                        <form action="//pixelo.us10.list-manage.com/subscribe/post?u=cd40064b8b57db2740e4382b8&amp;id=c67e7c18f7" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                            <div class="row">
                            	<div class="col-md-8">
                                    <div class="mc-field-group">
                                    	<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email Address">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="btn btn-primary btn-sm">
                                </div>
                            </div>
                        </form>
                        </div>
                        </div>
                        
                        <!--End mc_embed_signup-->

					</div>
					<div class="col-sm-2"></div>
				</div>
			</div>
		</div> <!-- .footer-widget -->
		
		<div class="site-info">
			<div class="container">
				<div class="row">
					<div class="col-sm-4 text-center">
						<div class="footer-logo">
															<a href="https://www.pixelo.net/">
									<img src="http://lembagawalinanggroe.id/assets/img/logo-header.png" alt="Pixelo" class="logo_standard" />
								</a>
																				</div>
						<p>Handpicked Design Deals and Bundles  for Creative Professionals.</p>
						<div class="social-icon">
							<a href="https://www.facebook.com/pixelodeals" target="_blank" class="wow zoomIn animated" data-wow-delay=".25s"><i class="fa fa-facebook" aria-hidden="true"></i></a>
							<a href="https://www.instagram.com/pixelodeals" target="_blank" class="wow zoomIn animated" data-wow-delay=".35s"><i class="fa fa-instagram" aria-hidden="true"></i></a>
							<a href="https://www.behance.net/pixelodeals" target="_blank" class="wow zoomIn animated" data-wow-delay=".55s"><i class="fa fa-behance" aria-hidden="true"></i></a>
						</div>
					</div>
					<div class="col-sm-1"></div>
					<div class="col-sm-2">
						<section id="nav_menu-2" class="footer-widget widget widget_nav_menu"><h3 class="widget-title">Company</h3><div class="menu-company-container"><ul id="menu-company" class="menu"><li id="menu-item-5030" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5030"><a href="https://www.pixelo.net/about/">About</a></li>
                            <li id="menu-item-1551" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1551"><a href="https://www.pixelo.net/blog/">Blog</a></li>
                            <li id="menu-item-1550" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1550"><a href="https://www.pixelo.net/affiliate/">Make Money</a></li>
                            <li id="menu-item-4779" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4779"><a href="https://www.pixelo.net/privacy-policy/">Privacy Policy</a></li>
                            <li class="user-cart menu-item menu-item-cart-item"><a class="cart-total" href="https://www.pixelo.net/cart/" title="View your shopping cart">Cart <span>0</span></a></li></ul></div>
                        </section>					
                    </div>
					<div class="col-sm-2">
                        <section id="nav_menu-8" class="footer-widget widget widget_nav_menu"><h3 class="widget-title">Product</h3><div class="menu-product-container"><ul id="menu-product" class="menu"><li id="menu-item-16160" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-16160"><a href="https://www.pixelo.net/bundle/">Current Bundle</a></li>
                            <li id="menu-item-5047" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-5047"><a href="https://www.pixelo.net/product-category/past-deals/">Past Deals</a></li>
                            <li id="menu-item-25406" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-25406"><a href="https://www.pixelo.net/product-category/freebies/">Freebies</a></li>
                            <li class="user-cart menu-item menu-item-cart-item"><a class="cart-total" href="https://www.pixelo.net/cart/" title="View your shopping cart">Cart <span>0</span></a></li></ul></div>
                        </section>					
                    </div>
					<div class="col-sm-2">
						<section id="nav_menu-3" class="footer-widget widget widget_nav_menu"><h3 class="widget-title">Support</h3><div class="menu-support-container"><ul id="menu-support" class="menu"><li id="menu-item-1557" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1557"><a href="https://www.pixelo.net/contact-us/">Contact</a></li>
                            <li id="menu-item-1556" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1556"><a href="https://www.pixelo.net/terms/">Terms</a></li>
                            <li id="menu-item-3297" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3297"><a href="https://www.pixelo.net/licenses/">Licenses</a></li>
                            <li class="user-cart menu-item menu-item-cart-item"><a class="cart-total" href="https://www.pixelo.net/cart/" title="View your shopping cart">Cart <span>0</span></a></li></ul></div>
                        </section>
                    </div>
				</div>
			</div>
		</div><!-- .site-info -->
		<div class="footer-copyright">
			<div class="container">
				<div class="row">
					<div class="mx-auto">
						<p>Copyright © 2016 — 2017 Pixelo. All Rights Reserved.</p>
					</div>
				</div>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<div class="back-to-top">
	<div id="sticky-bar">
		<a href="#"><i class="fa fa-angle-up"></i></a>
	</div>
</div>


<script type="text/javascript" src="./assets/js/custom.js"></script>
</body>
</html>