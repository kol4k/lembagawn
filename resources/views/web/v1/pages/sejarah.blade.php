@extends('web.v1.layouts.app')

@section('contentselector')
<div class="tengah2">
    <div class="kiri2">
        <div style="background-color:#deaf42; padding:1px; margin-top: 20px; border-radius:0 100px 0 0;">
            <h3 style="margin:2px 10px 5px 29px;">SEJARAH</h3>
        </div>
        <ul class="menu-pages">
			<a href="https://lembagawalinanggroe.id/v3">HOME</a>
            @foreach($data as $list)
            <a href="#" onClick="getPage({{ $list->id_sejarah }})"><li style="margin-bottom: -5px;" class="border-bawah"><b style="font-size:24px; color:#deaf42;">&#183;  </b>{{ $data->aceh_kategori->kategori_nama }}</li></a>
            @endforeach
        </ul>
    </div>
    <div class="kanan2">
        <div class="konten-nya" id="tengahkon">
				// Content in here	
        </div>
    </div>
</div>
@endsection