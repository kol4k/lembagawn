@extends('layouts.app')

@section('js')
<script>
function getLihat(id) {
		$('#modalLihat').html('<center><img src="https://upload.wikimedia.org/wikipedia/commons/b/b1/Loading_icon.gif"/></center>');
		jQuery.ajax({
			url: "modal/gallery/lihat/" + id,
			type: "GET",
			success:
                function(data){
                    $('#modalLihat').html(data);
                    console.log("ok lihat");
                }
		});
	}
function getEdit(id) {
		$('#modalEdit').html('<center><img src="https://upload.wikimedia.org/wikipedia/commons/b/b1/Loading_icon.gif"/></center>');
		jQuery.ajax({
			url: "modal/gallery/edit/" + id,
			type: "GET",
			success:
                function(data){
                    $('#modalEdit').html(data);
                    console.log("ok edit");
                }
		});
	}
</script>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-14">
            <div class="panel panel-default">
                <div class="panel-heading">Gallery  <a class="btn btn-primary btn-xs pull-right" data-toggle="modal" data-target="#myModal" role="button">Tambah gallery</a></div>

                <div class="panel-body">
                    
              <table id="example2" class="table table-hover">
                <thead>
                <tr>
                  <th width="1px;">No</th>
                  <th>Keterangan</th>
                  <th>Date</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data as $list)
                <tr>
                  <td>{{ $list->id_gallery }}</td>
                  <td>{{ $list->gallery_keterangan }}</td>
                  <td>{{ $list->created_at }}</td>
                  <td>
                  <a class="btn btn-success btn-xs" data-toggle="modal" data-target="#modalLihat" onclick="getLihat({{ $list->id_gallery }})" role="button">Lihat</a>
                  <a class="btn btn-warning btn-xs" data-toggle="modal" data-target="#modalEdit" onclick="getEdit({{ $list->id_gallery }})" role="button">Edit</a>
                  <a href="{{ route('gallery.delete',$list->id_gallery) }}" class="btn btn-danger btn-xs">Hapus</a>
                  </td>
                </tr>
                @endforeach
                </tbody>
              </table>
                    <!-- Trigger the modal with a button -->
                    <!-- Mod Edit -->
                    <!-- Modal -->
                    <div class="modal fade" id="modalLihat" role="dialog">
                    </div>
                    <div class="modal fade" id="modalEdit" role="dialog">
                    </div>
                    <!-- End -->

                    <!-- Modal -->
                    <div class="modal fade" id="myModal" role="dialog">
                        <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Tambah gallery</h4>
                            </div>
                            <div class="modal-body">
                            <form class="form-horizontal" id="gallery" method="POST" action="{{ route('gallery.post') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="email" class="col-md-3 control-label">Keterangan</label>

                                <div class="col-md-7">
                                    <textarea class="form-control" name="keterangan" rows="3"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="files" class="col-md-3 control-label">File input</label>
                                
                                <div class="col-md-7">
                                    <input type="file" class="form-control-file" id="files" name="files" aria-describedby="fileHelp">
                                    <small id="fileHelp" class="form-text text-muted">Hanya diperbolehkan file bertipe.</small>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="album" class="col-md-3 control-label">Album</label>

                                <div class="col-md-4">
                                <select class="form-control" id="album" name="album">
                                @foreach($albums as $album)
                                    <option value="{{ $album->id_album }}">{{ $album->album_judul }}</option>
                                @endforeach
                                </select>
                                </div> 
                            </div>

                            </div>
                            <div class="modal-footer">
                            <button type="submit" class="btn btn-default" form="gallery">Submit</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                            </form>
                        </div>
                        </div>
                    </div>
                    <!-- End Modal -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
