@extends('layouts.app')

@section('js')
<script>
function getLihat(id) {
		$('#modalLihat').html('<center><img src="https://upload.wikimedia.org/wikipedia/commons/b/b1/Loading_icon.gif"/></center>');
		jQuery.ajax({
			url: "modal/sejarah/lihat/" + id,
			type: "GET",
			success:
                function(data){
                    $('#modalLihat').html(data);
                    console.log("ok lihat");
                }
		});
	}
function getEdit(id) {
		$('#modalEdit').html('<center><img src="https://upload.wikimedia.org/wikipedia/commons/b/b1/Loading_icon.gif"/></center>');
		jQuery.ajax({
			url: "modal/sejarah/edit/" + id,
			type: "GET",
			success:
                function(data){
                    $('#modalEdit').html(data);
                    console.log("ok edit");
                }
		});
	}
</script>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-14">
            <div class="panel panel-default">
                <div class="panel-heading">Sejarah  <a class="btn btn-primary btn-xs pull-right" data-toggle="modal" data-target="#myModal" role="button">Tambah Sejarah</a></div>

                <div class="panel-body">
                    
              <table id="example2" class="table table-hover">
                <thead>
                <tr>
                  <th width="1px;">No</th>
                  <th>Judul</th>
                  <th>Kategori</th>
                  <th>Date</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data as $list)
                <tr>
                  <td>{{ $i=1, $i++ }}</td>
                  <td>{{ $list->sejarah_judul }}</td>
                  <td>{{ $list->aceh_kategori->kategori_nama }}
                  <td>{{ $list->created_at }}</td>
                  <td>
                  <a class="btn btn-success btn-xs" data-toggle="modal" data-target="#modalLihat" onclick="getLihat({{ $list->id_sejarah }})" role="button">Lihat</a>
                  <a class="btn btn-warning btn-xs" data-toggle="modal" data-target="#modalEdit" onclick="getEdit({{ $list->id_sejarah }})" role="button">Edit</a>
                  <a href="{{ route('sejarah.delete',$list->id_sejarah) }}" class="btn btn-danger btn-xs">Hapus</a>
                  </td>
                </tr>
                @endforeach
                </tbody>
              </table>
                    <!-- Trigger the modal with a button -->
                    <!-- Mod Edit -->
                    <!-- Modal -->
                    <div class="modal fade" id="modalLihat" role="dialog">
                    </div>
                    <div class="modal fade" id="modalEdit" role="dialog">
                    </div>
                    <!-- End -->

                    <!-- Modal -->
                    <div class="modal fade" id="myModal" role="dialog">
                        <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Tambah sejarah</h4>
                            </div>
                            <div class="modal-body">
                            <form class="form-horizontal" id="sejarah" method="POST" action="{{ route('sejarah.post') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="email" class="col-md-3 control-label">Judul</label>

                                <div class="col-md-7">
                                    <input id="nama" type="text" class="form-control" name="nama" required autofocus>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="email" class="col-md-3 control-label">Sejarah</label>

                                <div class="col-md-7">
                                    <textarea id="editor1" name="isi"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="kategori" class="col-md-3 control-label">Kategori</label>

                                <div class="col-md-4">
                                <select class="form-control" id="kategori" name="kategori">
                                @foreach($kategori as $kat)
                                    <option value="{{ $kat->id_kategori }}">{{ $kat->kategori_nama }}</option>
                                @endforeach
                                </select>
                                </div> 
                            </div>

                            <div class="form-group">
                                <label for="gambar" class="col-md-3 control-label">Upload Gambar</label>
                                
                                <div class="col-md-7">
                                    <input id="gambar" name="gambar" type="file">
                                </div>
                            </div>
                            
                            </div>
                            <div class="modal-footer">
                            <button type="submit" class="btn btn-default" form="sejarah">Submit</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                            </form>
                        </div>
                        </div>
                    </div>
                    <!-- End Modal -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
