@extends('layouts.app')

@section('js')
<script>
function getEdit(id) {
		$('#modalEdit').html('<center><img src="https://upload.wikimedia.org/wikipedia/commons/b/b1/Loading_icon.gif"/></center>');
		jQuery.ajax({
			url: "../modal/kategorisejarah/edit/" + id,
			type: "GET",
			success:
                function(data){
                    $('#modalEdit').html(data);
                    console.log("ok edit");
                }
		});
	}
</script>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-14">
            <div class="panel panel-default">
                <div class="panel-heading">Kategori Sejarah  <a class="btn btn-primary btn-xs pull-right" data-toggle="modal" data-target="#myModal" role="button">Tambah Kategori</a></div>

                <div class="panel-body">
                    
              <table id="example2" class="table table-hover">
                <thead>
                <tr>
                  <th width="1px;">No</th>
                  <th>Judul</th>
                  <th>Date</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data as $list)
                <tr>
                  <td>{{ $i=1, ++$i }}</td>
                  <td>{{ $list->kategori_nama }}</td>
                  <td>{{ $list->created_at }}</td>
                  <td>
                  <a class="btn btn-warning btn-xs" data-toggle="modal" data-target="#modalEdit" onclick="getEdit({{ $list->id_kategori }})" role="button">Edit</a>
                  <a href="{{ route('kategorisejarah.delete',$list->id_kategori ) }}" class="btn btn-danger btn-xs">Hapus</a>
                  </td>
                </tr>
                @endforeach
                </tbody>
              </table>
                    <!-- Trigger the modal with a button -->
                    <!-- Mod Edit -->
                    <!-- Modal -->
                    <div class="modal fade" id="modalLihat" role="dialog">
                    </div>
                    <div class="modal fade" id="modalEdit" role="dialog">
                    </div>
                    <!-- End -->

                    <!-- Modal -->
                    <div class="modal fade" id="myModal" role="dialog">
                        <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Tambah Kategori</h4>
                            </div>
                            <div class="modal-body">
                            <form class="form-horizontal" id="kategorisejarah" method="POST" action="{{ route('kategorisejarah.post') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="email" class="col-md-3 control-label">Nama</label>

                                <div class="col-md-7">
                                    <input id="nama" type="text" class="form-control" name="nama" required autofocus>
                                </div>
                            </div>
                            
                            </div>
                            <div class="modal-footer">
                            <button type="submit" class="btn btn-default" form="kategorisejarah">Submit</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                            </form>
                        </div>
                        </div>
                    </div>
                    <!-- End Modal -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
