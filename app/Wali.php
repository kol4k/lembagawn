<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wali extends Model
{
    protected $fillable = [
        'wali',
    ];
    protected $table = 'aceh_wali';
    protected $primaryKey = 'id_wali';
}
