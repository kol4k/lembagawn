<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Album;

class Gallery extends Model
{
    protected $table = 'aceh_gallery';
    protected $primaryKey = 'id_gallery';
    protected $fillable = [
        'id_album','gallery_keterangan','gallery_files'
    ];

    public function album()
    {
        return $this->belongsTo('App\Album','id_album');
    }
}
