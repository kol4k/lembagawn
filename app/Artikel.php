<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Artikel extends Model
{
    protected $fillable = [
        'artikel_judul', 'artikel_isi', 'artikel_gambar',
    ];
    protected $table = 'aceh_artikel';
    protected $primaryKey = 'id_artikel';
}
