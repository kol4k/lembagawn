<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profil extends Model
{
    protected $table = 'aceh_profil';
    protected $primaryKey = 'id_profil';
    protected $fillable = [
        'profil_judul','profil_isi','profil_gambar'
    ];
}
