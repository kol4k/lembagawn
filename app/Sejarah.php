<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\KategoriSejarah;

class Sejarah extends Model
{
    protected $table = 'aceh_sejarah';
    protected $primaryKey = 'id_sejarah';
    protected $fillable = [
        'id_kategori','sejarah_judul','sejarah_isi','sejarah_gambar'
    ];

    public function aceh_kategori()
    {
        return $this->hasOne('App\KategoriSejarah','id_kategori');
    }
}
