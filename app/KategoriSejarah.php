<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Sejarah;

class KategoriSejarah extends Model
{
    use softDeletes;
    
    protected $table = 'aceh_kategori';
    protected $primaryKey = 'id_kategori';
    protected $fillable = [
        'kategori_nama'
    ];
    protected $dates = ['deleted_at'];

    public function aceh_sejarah()
    {
        return $this->belongsTo('App\Sejarah','id_sejarah');
    }
}
