<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Gallery;

class Album extends Model
{
    protected $table = 'aceh_album';
    protected $primaryKey = 'id_album';
    protected $fillable = [
        'album_judul','album_keterangan'
    ];

    public function Gallery()
    {
        return $this->hasMany('App/Gallery','id_gallery');
    }
}
