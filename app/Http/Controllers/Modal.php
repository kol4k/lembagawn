<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers;
use App\Budaya;
use App\Artikel;

class Modal extends Controller
{
    public function lihatArtikel($id)
    {
        $artikel_helper = new CRUD\ArtikelController();
        return view('modal.artikel.lihat',['data' => $artikel_helper->show($id)]);
    }
    public function editArtikel($id)
    {
        $artikel_helper = new CRUD\ArtikelController();
        return view('modal.artikel.edit',['data' => $artikel_helper->show($id)]);
    }

    public function lihatBudaya($id)
    {
        $budaya_helper = new CRUD\BudayaController();
        return view('modal.budaya.lihat',['data' => $budaya_helper->show($id)]);
    }
    public function editBudaya($id)
    {
        $budaya_helper = new CRUD\BudayaController();
        return view('modal.budaya.edit',['data' => $budaya_helper->show($id)]);
    }

    public function lihatProfil($id)
    {
        $profil_helper = new CRUD\ProfilController();
        return view('modal.profil.lihat',['data' => $profil_helper->show($id)]);
    }
    public function editProfil($id)
    {
        $profil_helper = new CRUD\ProfilController();
        return view('modal.profil.edit',['data' => $profil_helper->show($id)]);
    }

    public function editKategoriSejarah($id)
    {
        $katSejarah_helper = new CRUD\KategoriSejarahController();
        return view('modal.kategorisejarah.edit',['data' => $katSejarah_helper->show($id)]);
    }

    public function lihatSejarah($id)
    {
        $sejarah_helper = new CRUD\SejarahController();
        return view('modal.sejarah.lihat',['data' => $sejarah_helper->show($id)]);
    }
    public function editSejarah($id)
    {
        $sejarah_helper = new CRUD\SejarahController();
        $kategori_helper = new CRUD\KategoriSejarahController();
        return view('modal.sejarah.edit',['data' => $sejarah_helper->show($id), 'kategori' => $kategori_helper->index()]);
    }

    public function lihatAlbum($id)
    {
        $album_helper = new CRUD\AlbumController();
        return view('modal.album.lihat',['data' => $album_helper->show($id)]);
    }
    public function editAlbum($id)
    {
        $album_helper = new CRUD\AlbumController();
        return view('modal.album.edit',['data' => $album_helper->show($id)]);
    }

    public function lihatGallery($id)
    {
        $gallery_helper = new CRUD\GalleryController();
        return view('modal.gallery.lihat',['data' => $gallery_helper->show($id)]);
    }
    public function editGallery($id)
    {
        $gallery_helper = new CRUD\GalleryController();
        return view('modal.gallery.edit',['data' => $gallery_helper->show($id)]);
    }
}
