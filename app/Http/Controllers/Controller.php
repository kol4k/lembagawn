<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public function doUpload($file,$folder='imgs'){
        $name = time().'.'.$file->getClientOriginalExtension(); // Time is default name
        $path = public_path('shared/'.$folder);
        if(!is_dir($path)){
            File::MakeDirectory($path,0777);
        }
        $file->move($path,$name);
        return $name;
    }

    public function doDocs($file,$folder='docs'){
        $name = time().'.'.$file->getClientOriginalExtension(); // Time is default name
        $path = public_path('shared/'.$folder);
        if(!is_dir($path)){
            File::MakeDirectory($path,0777);
        }
        $file->move($path,$name);
        return $name;
    }

}
