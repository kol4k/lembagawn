<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class VisitorOneController extends Controller
{
    public function __construct()
    {
        $this->sejarahClass = new CRUD\SejarahController();
    }
    public function sejarah()
    {
        $data = $this->sejarahClass;
        return view('web.v1.pages.sejarah',['data' => $data->index()]);
    }
}
