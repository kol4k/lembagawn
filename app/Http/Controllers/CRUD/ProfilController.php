<?php

namespace App\Http\Controllers\CRUD;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Profil;

class ProfilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Profil::all();
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $profil = new Profil();
        $profil->profil_judul = $request->nama;
        $profil->profil_isi = $request->isi;
        if($request->hasFile('gambar') && $request->file('gambar')->isValid()){
            $title = $this->doUpload($request->file('gambar'));
            $profil->profil_gambar = $title;
        }
        else{
            $profil->profil_gambar = 'not verif';
        }

        $profil->save();
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Profil::findOrFail($id);
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $profil = Profil::findOrFail($id);
        $profil->profil_judul = $request->nama;
        $profil->profil_isi = $request->isi;
        if($request->hasFile('gambar') && $request->file('gambar')->isValid()){
            $title = $this->doUpload($request->file('gambar'));
            $profil->profil_gambar = $title;
        }

        $profil->save();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $profil = Profil::findOrFail($id);
        $profil->delete();
        return redirect()->back();
    }

    public function page()
    {
        $data = $this->index();
        return view('admin.profil',['data' => $data]);
    }
}
