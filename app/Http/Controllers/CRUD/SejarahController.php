<?php

namespace App\Http\Controllers\CRUD;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Sejarah;

class SejarahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Sejarah::all();
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sejarah = new Sejarah;
        $sejarah->id_kategori = $request->kategori;
        $sejarah->sejarah_judul = $request->nama;
        $sejarah->sejarah_isi = $request->isi;
        if($request->hasFile('gambar') && $request->file('gambar')->isValid()){
            $title = $this->doUpload($request->file('gambar'));
            $sejarah->sejarah_gambar = $title;
        }
        else{
            $sejarah->sejarah_gambar = 'not verif';
        }
        $sejarah->save();
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Sejarah::findOrFail($id);
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sejarah = Sejarah::findOrFail($id);
        $sejarah->id_kategori = $request->kategori;
        $sejarah->sejarah_judul = $request->nama;
        $sejarah->sejarah_isi = $request->isi;
        if($request->hasFile('gambar') && $request->file('gambar')->isValid()){
            $title = $this->doUpload($request->file('gambar'));
            $sejarah->sejarah_gambar = $title;
        }
        $sejarah->save();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Sejarah::findOrFail($id);
        $sejarah->delete();
        return redirect()->back();
    }

    public function page() 
    {
        $data = $this->index();
        $kategori_helper = new KategoriSejarahController();
        return view('admin.sejarah',['data' => $data,'kategori' => $kategori_helper->index()]);
    }
    public function testCon($id)
    {
        $data = $this->show($id);
        return dd($data->aceh_kategori->kategori_nama);
    }
}
