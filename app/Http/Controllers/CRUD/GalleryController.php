<?php

namespace App\Http\Controllers\CRUD;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Gallery;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Gallery::all();
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $gallery = new Gallery();
        $gallery->id_album = $request->album;
        $gallery->gallery_keterangan = $request->keterangan;
        if($request->hasFile('files') && $request->file('files')->isValid()){
            $title = $this->doDocs($request->file('files'));
            $gallery->gallery_files = $title;
        }
        else{
            $gallery->gallery_files = 'not verif';
        }
        $gallery->save();
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Gallery::findOrFail($id);
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $gallery = Gallery::findOrFail($id);
        $gallery->id_album = $request->album;
        $gallery->gallery_keterangan = $request->keterangan;
        if($request->hasFile('files') && $request->file('files')->isValid()){
            $title = $this->doDocs($request->file('files'));
            $gallery->gallery_files = $title;
        }
        else{
            $gallery->gallery_files = 'not verif';
        }
        $gallery->save();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gallery = Gallery::findOrFail($id);
        $gallery->delete();
        return redirect()->back();
    }

    public function page() 
    {
        $data = $this->index();
        $album_helper = new AlbumController();
        return view('admin.gallery',['data' => $data,'albums' => $album_helper->index()]);
    }
}
