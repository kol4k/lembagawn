<?php

namespace App\Http\Controllers\CRUD;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Artikel;

class ArtikelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $artikel = Artikel::all();
        return $artikel;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $artikel = new Artikel();
        $artikel->artikel_judul = $request->nama;
        $artikel->artikel_isi = $request->isi;
        if($request->hasFile('gambar') && $request->file('gambar')->isValid()){
            $title = $this->doUpload($request->file('gambar'));
            $artikel->artikel_gambar = $title;
        }
        else{
            $artikel->artikel_gambar = 'not verif';
        }

        $artikel->save();
        // return var_dump($request->file('gambar'));
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Artikel::findOrFail($id);
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $artikel = Artikel::FindOrFail($id);
        $artikel->artikel_judul = $request->nama;
        $artikel->artikel_isi = $request->isi;
        if($request->hasFile('gambar') && $request->file('gambar')->isValid()){
            $title = $this->doUpload($request->file('gambar'));
            $artikel->artikel_gambar = $title;
        }

        $artikel->save();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // return $id;
        $data = Artikel::findOrFail($id);
        // return var_dump($data);
        $data =  $data->delete();
        return redirect()->back();
    }

    public function page(){
        $data = $this->index();
        // return $data;
        return view('admin.artikel',['data' => $data]);
    }
}
