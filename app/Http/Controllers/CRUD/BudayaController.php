<?php

namespace App\Http\Controllers\CRUD;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Budaya;

class BudayaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Budaya::all();
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $budaya = new Budaya();
        $budaya->budaya_judul = $request->nama;
        $budaya->budaya_isi = $request->isi;
        if($request->hasFile('gambar') && $request->file('gambar')->isValid()){
            $title = $this->doUpload($request->file('gambar'));
            $budaya->budaya_gambar = $title;
        }
        else{
            $budaya->budaya_gambar = 'not verif';
        }

        $budaya->save();
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Budaya::findOrFail($id);
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $budaya = Budaya::findOrFail($id);
        $budaya->budaya_judul = $request->nama;
        $budaya->budaya_isi = $request->isi;
        if($request->hasFile('gambar') && $request->file('gambar')->isValid()){
            $title = $this->doUpload($request->file('gambar'));
            $budaya->budaya_gambar = $title;
        }

        $budaya->save();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $budaya = Budaya::findOrFail($id);
        $budaya->delete();
        return redirect()->back();
    }

    public function page(){
        $data = $this->index();
        // return $data;
        return view('admin.budaya',['data' => $data]);
    }
}
