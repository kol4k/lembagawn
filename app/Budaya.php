<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Budaya extends Model
{
    protected $fillable = [
        'budaya_judul', 'budaya_isi', 'budaya_gambar',
    ];
    protected $table = 'aceh_budaya';
    protected $primaryKey = 'id_budaya';
}
