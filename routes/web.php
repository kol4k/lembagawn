<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/pass', function () {
    return bcrypt('root123');
});

Route::get('sejarah', 'VisitorOneController@sejarah')->name('index.Sejarah');

Auth::routes();
Route::group(['prefix' => '/admin'], function () {
    Route::get('test/{id}', 'CRUD\SejarahController@testCon');
    Route::group(['prefix' => '/crud'], function () {
    Route::group(['prefix' => '/artikel'], function () {
        Route::post('/post', 'CRUD\ArtikelController@store')->name('artikel.post');
        Route::post('/edit/{id}', 'CRUD\ArtikelController@update')->name('artikel.edit');
        Route::get('/delete/{id}', 'CRUD\ArtikelController@destroy')->name('artikel.delete');
    });
    
    Route::group(['prefix' => '/budaya'], function () {
        Route::post('/post', 'CRUD\BudayaController@store')->name('budaya.post');
        Route::post('/edit/{id}', 'CRUD\BudayaController@update')->name('budaya.edit');
        Route::get('/delete/{id}', 'CRUD\BudayaController@destroy')->name('budaya.delete');
    });

    Route::group(['prefix' => '/profil'], function () {
        Route::post('/post', 'CRUD\ProfilController@store')->name('profil.post');
        Route::post('/edit/{id}', 'CRUD\ProfilController@update')->name('profil.edit');
        Route::get('/delete/{id}', 'CRUD\ProfilController@destroy')->name('profil.delete');
    });

    Route::group(['prefix' => '/kategorisejarah'], function () {
        Route::post('/post', 'CRUD\KategoriSejarahController@store')->name('kategorisejarah.post');
        Route::post('/edit/{id}', 'CRUD\KategoriSejarahController@update')->name('kategorisejarah.edit');
        Route::get('/delete/{id}', 'CRUD\KategoriSejarahController@destroy')->name('kategorisejarah.delete');
    });

    Route::group(['prefix' => '/sejarah'], function () {
        Route::post('/post', 'CRUD\SejarahController@store')->name('sejarah.post');
        Route::post('/edit/{id}', 'CRUD\SejarahController@update')->name('sejarah.edit');
        Route::get('/delete/{id}', 'CRUD\SejarahController@destroy')->name('sejarah.delete');
    });

    Route::group(['prefix' => '/album'], function () {
        Route::post('/post', 'CRUD\AlbumController@store')->name('album.post');
        Route::post('/edit/{id}', 'CRUD\AlbumController@update')->name('album.edit');
        Route::get('/delete/{id}', 'CRUD\AlbumController@destroy')->name('album.delete');
    });

    Route::group(['prefix' => '/gallery'], function () {
        Route::post('/post', 'CRUD\GalleryController@store')->name('gallery.post');
        Route::post('/edit/{id}', 'CRUD\GalleryController@update')->name('gallery.edit');
        Route::get('/delete/{id}', 'CRUD\GalleryController@destroy')->name('gallery.delete');
    });
});

Route::group(['prefix' => '/modal'], function(){
    Route::group(['prefix' => '/artikel'], function(){
        Route::get('lihat/{id}', 'Modal@lihatArtikel')->name('modal.artikel.lihat');
        Route::get('edit/{id}', 'Modal@editArtikel')->name('modal.artikel.edit');
    });
    
    Route::group(['prefix' => '/budaya'], function(){
        Route::get('lihat/{id}', 'Modal@lihatBudaya')->name('modal.budaya.lihat');
        Route::get('edit/{id}', 'Modal@editBudaya')->name('modal.budaya.edit');
    });

    Route::group(['prefix' => '/profil'], function(){
        Route::get('lihat/{id}', 'Modal@lihatProfil')->name('modal.profil.lihat');
        Route::get('edit/{id}', 'Modal@editProfil')->name('modal.profil.edit');
    });

    Route::group(['prefix' => '/kategorisejarah'], function(){
        Route::get('edit/{id}', 'Modal@editKategoriSejarah')->name('modal.kategorisejarah.edit');
    });

    Route::group(['prefix' => '/sejarah'], function(){
        Route::get('lihat/{id}', 'Modal@lihatSejarah')->name('modal.sejarah.lihat');
        Route::get('edit/{id}', 'Modal@editSejarah')->name('modal.sejarah.edit');
    });

    Route::group(['prefix' => '/album'], function(){
        Route::get('lihat/{id}', 'Modal@lihatAlbum')->name('modal.album.lihat');
        Route::get('edit/{id}', 'Modal@editAlbum')->name('modal.album.edit');
    });

    Route::group(['prefix' => '/gallery'], function(){
        Route::get('lihat/{id}', 'Modal@lihatGallery')->name('modal.gallery.lihat');
        Route::get('edit/{id}', 'Modal@editGallery')->name('modal.gallery.edit');
    });
});
Route::get('/', 'HomeController@index')->name('home');
Route::get('artikel', 'CRUD\ArtikelController@page')->name('Artikel');
Route::get('budaya', 'CRUD\BudayaController@page')->name('Budaya');
Route::get('profil', 'CRUD\ProfilController@page')->name('Profil');
Route::get('sejarah', 'CRUD\SejarahController@page')->name('Sejarah');
Route::get('album', 'CRUD\AlbumController@page')->name('Album');
Route::get('gallery', 'CRUD\GalleryController@page')->name('Gallery');

Route::group(['prefix' => 'kategori'], function (){
    Route::get('sejarah', 'CRUD\KategoriSejarahController@page')->name('KategoriSejarah');
});

});
